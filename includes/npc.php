<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

include_once "mapper.php";

class Npc extends Cache {

	protected $_npc;
	protected $db;

	public $id;
	public $name;

	/**
	 * @param PDO database handler
	 * @param integer entry of npc
	 */
	function __construct($db,$id) {
		$this->db = $db;
		$this->id = $id;

		// search for cached data. Set variable and stop processing when found.
		if ($this->_npc = $this->get_cache(array('npc',$id),NPC_EXPIRE)) {
			return;
		}
		
		$get_npc = $this->db->query('
			SELECT ct.`entry`,ct.`name`,ct.`faction_A`,ct.`faction_H`,ct.`npcflag`,ct.`unit_flags`,ct.`unit_flags2`,ct.`family`
			FROM `'.$this->db->worlddb.'`.`creature_template` AS ct
			WHERE ct.`entry` = ?',
			array($id)
		);

		if ($get_npc->rowCount() == 1) {
			$this->_npc = $get_npc->fetch(PDO::FETCH_ASSOC);
			$quests = new Quests($this->db);
			$this->_npc['starter'] = $quests->search_by_npc_starter($id);
			$this->_npc['ender'] = $quests->search_by_npc_ender($id);
			$loot = new Loot($this->db,'creature',$id);
			$this->_npc['loot'] = $loot->get_loot_table();

			$this->store_cache(array('npc',$id),$this->_npc);
		}
	}

	/**
	 * Returns npc informations
	 * @return array npc informations
	 */
	public function get_npc() {
		if (!$this->_npc['entry'])
			return;

		return $this->_npc;
	}

	/**
	 * Returns npc name
	 * @return string npc name
	 */
	public function get_name() {
		return $this->_npc['name'];
	}


	public function get_spawns() {

		$spawns = array();

		// search for cached data. Set variable and stop processing when found.
		if ($spawns = $this->get_cache(array('npc_spawns',$this->id),NPC_EXPIRE)) {
			return $spawns;
		}

		$get_spawns = $this->db->query('
			SELECT guid,map,position_x,position_y,spawntimesecs
			FROM `'.$this->db->worlddb.'`.`creature`
			WHERE id=?',
			array($this->id)
		);

		foreach ($get_spawns->fetchAll(PDO::FETCH_ASSOC) as $s) {
			$mapper = new Mapper($this->db,$s['map']);
			$positions = $mapper->check_spawn($s['position_x'],$s['position_y']);
			foreach ($positions as $p) {
				$spawns[$p[0]][] = array($s['guid'],$p[1],$p[2]);
			}
		}

		$this->store_cache(array('npc_spawns',$this->id),$spawns);

		return $spawns;
	}
}














class Spawn extends Cache {

	protected $_spawn;
	protected $db;

	public $guid;

	/**
	 * @param PDO database handler
	 * @param integer entry of npc
	 */
	function __construct($db,$guid) {
		$this->db = $db;
		$this->guid = $guid;

		// search for cached data. Set variable and stop processing when found.
		if ($this->_spawn = $this->get_cache(array('spawn',$id),NPC_EXPIRE)) {
			return;
		}
		
		$get_npc = $this->db->query('
			SELECT `position_x`,`position_y`,`spawntimesecs`
			FROM `'.$this->db->worlddb.'`.`creature`
			WHERE `guid` = ?',
			array($guid)
		);

		if ($get_npc->rowCount() == 1) {
			$this->_spawn = $get_npc->fetch(PDO::FETCH_ASSOC);
			$this->store_cache(array('spawn',$id),$this->_spawn);
		}
	}

	/**
	 * Returns creature spawn informations
	 * @return array creature spawn informations
	 */
	public function get_spawn() {
		if (!$this->_spawn)
			return;

		return $this->_spawn;
	}

}
