<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

class Items extends Cache {

	protected $db;

	/**
	 * Initialize new search
	 */
	function __construct($db) {
		$this->db = $db;
	}
	
	/**
	 * Search in items
	 * @return array items list
	 */
	public function search_by_name($name) {
		$get_items = $this->db->query('
			SELECT dis.`col_0` AS entry,dis.`col_9` AS inventoryType,dis.`col_12` AS itemLevel,dis.`col_13` AS requiredLevel,dis.`col_99` AS name,LOWER(idi.`col_5`) AS icon
			FROM `db2_item_sparse` AS dis
			LEFT JOIN `db2_item` AS di ON (dis.`col_0`=di.`col_0`)
			LEFT JOIN `dbc_itemdisplayinfo` AS idi ON (di.`col_5`=idi.`col_0`)
			WHERE dis.`col_99` COLLATE utf8_general_ci LIKE ? LIMIT '.SQL_LIMIT,		// consider adding fulltext over name filed in mysql and use AGAINST
			array('%'.$name.'%')
		);

		return $get_items->fetchAll(PDO::FETCH_ASSOC);
	}
}
