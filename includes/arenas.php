<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

class Arenateams extends Cache {

	protected $db;

	/**
	* Initialize new search
	*/
	function __construct($db) {
		$this->db = $db;
	}
	
	/**
	 * Search in arena teams
	 * @return array arenateams list
	 */
	public function search_by_name($name) {
		$get_teams = $this->db->query('
			SELECT chat.`arenaTeamId`,chat.`name` AS arenateamName,chat.`type`,chat.`rating`,chat.`seasonGames`,chat.`seasonWins`,chat.`weekGames`,chat.`weekWins`,chat.`rank`,ch.`name` AS captainName,ch.`race` AS captainRace
			FROM `'.$this->db->characterdb.'`.`arena_team` AS chat
			LEFT JOIN `'.$this->db->characterdb.'`.`characters` AS ch ON (chat.`captainGuid`=ch.`guid`)
			WHERE chat.`name` LIKE ? LIMIT '.SQL_LIMIT,	// consider adding fulltext over name filed in mysql and use AGAINST
			array('%'.$name.'%')
		);

		return $get_teams->fetchAll(PDO::FETCH_ASSOC);
	}

}
