<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


define('ITEM_CLASS_WEAPON', 2);
define('ITEM_CLASS_ARMOR', 4);
define('ITEM_QUALITY_ARTIFACT', 6);

define('ITEM_SUBCLASS_ARMOR_CLOTH', 1);
define('ITEM_SUBCLASS_WEAPON_BOW', 2);
define('ITEM_SUBCLASS_WEAPON_GUN', 3);
define('ITEM_SUBCLASS_ARMOR_SHIELD', 6);
define('ITEM_SUBCLASS_WEAPON_THROWN', 16);
define('ITEM_SUBCLASS_WEAPON_CROSSBOW', 18);
define('ITEM_SUBCLASS_WEAPON_WAND', 19);

define('INVTYPE_HEAD', 1);
define('INVTYPE_NECK', 2);
define('INVTYPE_SHOULDERS', 3);
define('INVTYPE_BODY', 4);
define('INVTYPE_CHEST', 5);
define('INVTYPE_WAIST', 6);
define('INVTYPE_LEGS', 7);
define('INVTYPE_FEET', 8);
define('INVTYPE_WRISTS', 9);
define('INVTYPE_HANDS', 10);
define('INVTYPE_FINGER', 11);
define('INVTYPE_TRINKET', 12);
define('INVTYPE_WEAPON', 13);
define('INVTYPE_SHIELD', 14);
define('INVTYPE_RANGED', 15);
define('INVTYPE_CLOAK', 16);
define('INVTYPE_2HWEAPON', 17);
define('INVTYPE_BAG', 18);
define('INVTYPE_TABARD', 19);
define('INVTYPE_ROBE', 20);
define('INVTYPE_WEAPONMAINHAND', 21);
define('INVTYPE_WEAPONOFFHAND', 22);
define('INVTYPE_HOLDABLE', 23);
define('INVTYPE_AMMO', 24);
define('INVTYPE_THROWN', 25);
define('INVTYPE_RANGEDRIGHT', 26);
define('INVTYPE_QUIVER', 27);
define('INVTYPE_RELIC', 28);


define('ITEM_FLAGS_EXTRA_CASTER_WEAPON', 512);


$shared = array();

$shared['SlotMod'] = array(
	0,		// not equipable
	1,		// 1 head
	0.5625,		// 2 Neck
	0.75,		// 3 Shoulders
	1,		// 4 Body
	1,		// 5 Chest
	0.75,		// 6 Waist
	1,		// 7 Legs
	0.75,		// 8 Feet
	0.5625,		// 9 Wrist
	0.75,		// 10 Hands
	0.5625,		// 11 Finger
	0.5625,		// 12 Trinket
	1,		// 13 Weapon
	1,		// 14 Shield
	0.3164,		// 15 Ranged
	0.5625,		// 16 Cloak
	2,		// 17 Two-Hand
	0,		// 18 Bag
	0,		// 19 Tabard
	1,		// 20 Robe
	1,		// 21 Main-Hand
	1,		// 22 Off-Hand
	1,		// 23 Holdable
	0,		// 24 Ammo
	0.3164,		// 25 Thrown
	0.3164,		// 26 Right-Hand Ranged
	0,		// 27 Quiver
	0.3164		// 28 Relic
);


$shared['formula'] = array(
	array(300, 1),			// 0 grey
	array(300, 1),			// 1 white
	array(91.45000, 0.65),		// 2 green
	array(91.45000, 0.65),		// 3 blue
	array(91.45000, 0.65),		// 4 purple
	array(91.45000, 0.65),		// 5 legendary
	array(91.45000, 0.65),		// 6 artifact
	array(91.45000, 0.65)		// 7 heriloom bta
);	


$shared['ItemBondingType'] = array(
	'',				// 0
	'Binds when picked up',		// 1
	'Binds when equipped',	// 2
	'Binds when used',		// 3
	'Quest Item',			// 4
);

$shared['InventoryType'] = array(
	'',			// 0
	'Head',			// 1
	'Neck',			// 2
	'Shoulders',		// 3
	'Body',			// 4
	'Chest',		// 5
	'Waist',		// 6
	'Legs',			// 7
	'Feet',			// 8
	'Wrist',		// 9
	'Hands',		// 10
	'Finger',		// 11
	'Trinket',		// 12
	'Weapon',		// 13
	'Off-Hand',		// 14 shield
	'Ranged',		// 15
	'Cloak',		// 16
	'Two-Hand',		// 17
	'Bag',			// 18
	'Tabard',		// 19
	'Robe',			// 20
	'Main-Hand',		// 21
	'Off-Hand',		// 22
	'Holdable',		// 23
	'Ammo',			// 24
	'Thrown',		// 25
	'Right-Hand Ranged',	// 26
	'Quiver',		// 27
	'Relic'			// 28
);

$shared['ItemClass'] = array(
	array( // consumable | 0,
		'Consumable', 		// 0
		'Potion',		// 1
		'Elixir',		// 2
		'Flask',		// 3
		'Scroll',		// 4
		'Food/Drink',		// 5
		'Item Enhancement',	// 6
		'Bandage',		// 7
		'Other Consumable'	// 8
	),
	array( // container | 1
		'Container',		// 0
		'Soul Bag',		// 1
		'Herbalism Bag',	// 2
		'Enchanting Bag',	// 3
		'Engineering Bag',	// 4
		'Gem Bag',		// 5
		'Mining Bag',		// 6
		'Leatherworking Bag',	// 7
		'Inscription Bag',	// 8
		'Tackle Bag',		// 9
	),
	array( // weapon | 2
		'Axe',			// 0
		'Axe',			// 1
		'Bow',			// 2
		'Gun',			// 3
		'Mace',			// 4
		'Mace',			// 5
		'Polearm',		// 6
		'Sword',		// 7
		'Sword',		// 8
		'',			// 9
		'Staff',		// 10
		'Exotic',		// 11
		'Exotic',		// 12
		'Fist',			// 13
		'Miscellaneous',	// 14
		'Dagger',		// 15
		'Thrown',		// 16
		'Spear',		// 17
		'Crossbow',		// 18
		'Wand',			// 19
		'Fishing Pole',		// 20
	),
	array( // gem | 3
		'Red',			// 0
		'Blue',			// 1
		'Yellow',		// 2
		'Purple',		// 3
		'Green',		// 4
		'Orange',		// 5
		'Meta',			// 6
		'Simple',		// 7
		'Prismatic',		// 8
		'Hydraulic',		// 9
		'Cogwheel',		// 10
	),
	array( // armor | 4
		'Miscellaneous',	// 0
		'Cloth',		// 1
		'Leather',		// 2
		'Mail', 		// 3
		'Plate', 		// 4
		'Buckler',		// 5
		'Shield',		// 6
		'Libram',		// 7
		'Idol', 		// 8
		'Totem',	 	// 9
		'Sigil',	 	// 10
		'Relic',	 	// 11
	),
	array( // reagent | 5
		'Reagent',		// 0

	),
	array( // projectile | 6
		'',			// 0
		'',			// 1
		'Arrow',		// 2
		'Bullet',		// 3
		'',			// 4
	),
/*	array( // 
		'', // 

	),
	array( // 
		'', // 

	),


    ITEM_CLASS_TRADE_GOODS                      = 7,
    ITEM_CLASS_GENERIC                          = 8,  // OBSOLETE
    ITEM_CLASS_RECIPE                           = 9,
    ITEM_CLASS_MONEY                            = 10, // OBSOLETE
    ITEM_CLASS_QUIVER                           = 11,
    ITEM_CLASS_QUEST                            = 12,
    ITEM_CLASS_KEY                              = 13,
    ITEM_CLASS_PERMANENT                        = 14, // OBSOLETE
    ITEM_CLASS_MISCELLANEOUS                    = 15,
    ITEM_CLASS_GLYPH                            = 16
*/
);


$shared['ItemModType'] = array(
	'Mana',				// 0 
	'Health',			// 1
	'',				// 2
	'Agility',			// 3
	'Strength',			// 4
	'Intelect',			// 5
	'Spirit',			// 6
	'Stamina',			// 7
	'',				// 8
	'',				// 9
	'',				// 10
	'',				// 11
	'Defense Rating',		// 12
	'Dodge Rating',			// 13
	'Parry Rating',			// 14
	'Block Rating',			// 15
	'Melee Hit Rating',		// 16
	'Ranged Hit Rating',		// 17
	'Spell Hit Rating',		// 18
	'Melee Crit Rating',		// 19
	'Ranged Crit Rating',		// 20
	'Spell Crit Rating',		// 21
	'HIT_TAKEN_MELEE_RATING',	// 22
	'HIT_TAKEN_RANGED_RATING',	// 23
	'HIT_TAKEN_SPELL_RATING',	// 24
	'CRIT_TAKEN_MELEE_RATING',	// 25
	'CRIT_TAKEN_RANGED_RATING',	// 26
	'CRIT_TAKEN_SPELL_RATING',	// 27
	'Melee Haste Rating',		// 28
	'Ranged Haste Rating',		// 29
	'Spell Haste Rating',		// 30
	'Hit Rating',			// 31
	'Crit Rating',			// 32
	'Hit Taken Rating',		// 33
	'Crit Taken Rating',		// 34
	'Resilience Rating',		// 35
	'Haste Rating',			// 36
	'Expertise Rating',		// 37
	'Attack Power',			// 38
	'Ranged Attack Power',		// 39
	'',				// 40
	'',				// 41
	'',				// 42
	'Mana Regeneration',		// 43
	'Armor Penetration Rating',	// 44
	'Spell Power',			// 45
	'Health Regeneration',		// 46
	'Spell Penetration',		// 47
	'Block Value',			// 48
	'Mastery Rating',		// 49
	'Extra Armor',			// 50
	'Fire Resistance',		// 51
	'Frost Resistance',		// 52
	'Holy Resistance',		// 53
	'Shadow Resistance',		// 54
	'Nature Resistance',		// 55
	'Arcane Resistance' 		// 56
);


$shared['SocketColor'] = array(
	'1' => 'meta',
	'2' => 'red',
	'4' => 'yellow',
	'6' => 'orange',
	'8' => 'blue',
	'10' => 'purple',
	'12' => 'green',
	'14' => 'prismatic',
	'16' => 'hydraulic',
	'32' => 'cogwheel'
);

$shared['Classes'] = array(
	'',		// 0
	'Warrior',	// 1
	'Paladin',	// 2
	'Hunter',	// 3
	'Rogue',	// 4
	'Priest',	// 5
	'Death Knight',	// 6
	'Shaman',	// 7
	'Mage',		// 8
	'Warlock',	// 9
	'',		// 10
	'Druid',	// 11
);

$shared['Races'] = array(
	'',		// 0
	'Human',	// 1
	'Orc',		// 2
	'Dwarf',	// 3
	'Night Elf',	// 4
	'Undead',	// 5
	'Tauren',	// 6
	'Gnome',	// 7
	'Troll',	// 8
	'Goblin',	// 9
	'Blood Elf',	// 10
	'Draenei',	// 11
	'',		// 12
	'',		// 13
	'',		// 14
	'',		// 15
	'',		// 16
	'',		// 17
	'',		// 18
	'',		// 19
	'',		// 20
	'',		// 21
	'Worgen',	// 22
	'',		// 23
);

$shared['racemaskAlliance'] = 2098253; // same as ((1<<(1-1)) | (1<<(3-1)) | (1<<(4-1)) | (1<<(7-1)) | (1<<(11-1)) | (1<<(22-1)));
$shared['racemaskHorde'] = 946; // same as ((1<<(2-1)) | (1<<(5-1)) | (1<<(6-1)) | (1<<(8-1)) | (1<<(9-1)) | (1<<(10-1)));


$shared['ItemColors'] = array(
	'#9d9d9d', 	// 0 poor - grey
	'#fff',		// 1 common - white
	'#1eff00',	// 2 uncommon - green
	'#0070ff',	// 3 rare - blue
	'#a335ee',	// 4 epic - purple
	'#ff8000',	// 5 legendary - orange
	'#e6cc80',	// 6 artifact - yellow
	'#e6cc80',	// 7 aheirloom - yellow
);


$shared['classColors'] = array(
	'',		// 0
	'#C79C6E',	// 1 warrior
	'#F58CBA',	// 2 paladin
	'#ABD473',	// 3 hunter
	'#FFF569',	// 4 rogue
	'#FFFFFF',	// 5 priest
	'#C41F3B',	// 6 deathknight
	'#0070DE',	// 7 shaman
	'#69CCF0',	// 8 mage
	'#9482C9',	// 9 warlock
	'',		// 10
	'#FF7D0A',	// 11 druid
);

$shared['qualityMultipliers'] = array(
	1.0,
	1.0,
	1.0,
	1.17,
	1.37,
	1.68,
	0.0,
	0.0
);

$shared['armorMultipliers'] = array(
	0.00, // INVTYPE_NON_EQUIP
	0.59, // INVTYPE_HEAD
	0.00, // INVTYPE_NECK
	0.59, // INVTYPE_SHOULDERS
	0.00, // INVTYPE_BODY
	1.00, // INVTYPE_CHEST
	0.35, // INVTYPE_WAIST
	0.75, // INVTYPE_LEGS
	0.49, // INVTYPE_FEET
	0.35, // INVTYPE_WRISTS
	0.35, // INVTYPE_HANDS
	0.00, // INVTYPE_FINGER
	0.00, // INVTYPE_TRINKET
	0.00, // INVTYPE_WEAPON
	1.00, // INVTYPE_SHIELD
	0.00, // INVTYPE_RANGED
	0.00, // INVTYPE_CLOAK
	0.00, // INVTYPE_2HWEAPON
	0.00, // INVTYPE_BAG
	0.00, // INVTYPE_TABARD
	1.00, // INVTYPE_ROBE
	0.00, // INVTYPE_WEAPONMAINHAND
	0.00, // INVTYPE_WEAPONOFFHAND
	0.00, // INVTYPE_HOLDABLE
	0.00, // INVTYPE_AMMO
	0.00, // INVTYPE_THROWN
	0.00, // INVTYPE_RANGEDRIGHT
	0.00, // INVTYPE_QUIVER
	0.00, // INVTYPE_RELIC
);

$shared['weaponMultipliers'] = array(
	0.89, // ITEM_SUBCLASS_WEAPON_AXE
	1.03, // ITEM_SUBCLASS_WEAPON_AXE2
	0.77, // ITEM_SUBCLASS_WEAPON_BOW
	0.77, // ITEM_SUBCLASS_WEAPON_GUN
	0.89, // ITEM_SUBCLASS_WEAPON_MACE
	1.03, // ITEM_SUBCLASS_WEAPON_MACE2
	1.03, // ITEM_SUBCLASS_WEAPON_POLEARM
	0.89, // ITEM_SUBCLASS_WEAPON_SWORD
	1.03, // ITEM_SUBCLASS_WEAPON_SWORD2
	0.00, // ITEM_SUBCLASS_WEAPON_Obsolete
	1.03, // ITEM_SUBCLASS_WEAPON_STAFF
	0.00, // ITEM_SUBCLASS_WEAPON_EXOTIC
	0.00, // ITEM_SUBCLASS_WEAPON_EXOTIC2
	0.64, // ITEM_SUBCLASS_WEAPON_FIST_WEAPON
	0.00, // ITEM_SUBCLASS_WEAPON_MISCELLANEOUS
	0.64, // ITEM_SUBCLASS_WEAPON_DAGGER
	0.64, // ITEM_SUBCLASS_WEAPON_THROWN
	0.00, // ITEM_SUBCLASS_WEAPON_SPEAR
	0.77, // ITEM_SUBCLASS_WEAPON_CROSSBOW
	0.64, // ITEM_SUBCLASS_WEAPON_WAND
	0.64, // ITEM_SUBCLASS_WEAPON_FISHING_POLE
);

$shared['guildRecruitmentInterests'] = array(
	1 => 'Questing',
	2 => 'Dungeons',
	4 => 'Raids',
	8 => 'PvP',
	16 => 'Role Playing',
);

$shared['guildRecruitmentAvailability'] = array(
	1 => 'Weekdays',
	2 => 'Weekends',
);

$shared['guildRecruitmentRoles'] = array(
	1 => 'Tank',
	2 => 'Healer',
	4 => 'Damage',
);

$shared['guildRecruitmentLevels'] = array(
	1 => 'Any',
	2 => 'Max',
);

function human_size($bytes, $precision = 2) {
    $unit = array('','K','M','G','T','P','E');
    return @round(
        $bytes / pow(1000, ($i = floor(log($bytes, 1000)))), $precision).' '.$unit[$i];
}
