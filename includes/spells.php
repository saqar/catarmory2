<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */



class Spells {

	protected $db;

	/**
	 * Initialize new search
	 */
	function __construct($db) {
		$this->db = $db;
	}

	/**
	 * Search in spells
	 * @return array spells list
	 */
	public function search_by_name($name) {

		// no cache here - lots of searching combinations
		// not using lookup via Spell() - consider using it - as spell is cached

		$get_spells = $this->db->query('
			SELECT ds.`col_0` AS id,ds.`col_21` AS name,ds.`col_22` AS rank,ds.`col_23` AS description,ds.`col_24` AS tooltip,REPLACE(LOWER(dsi.`col_1`),"interface\\\\icons\\\\","") AS icon
			FROM `dbc_spell` AS ds
			LEFT JOIN `dbc_spellicon` AS dsi ON (ds.`col_19`=dsi.col_0)
			WHERE ds.`col_21` LIKE ? LIMIT '.SQL_LIMIT,		// consider adding fulltext over name filed in mysql and use AGAINST
			array('%'.$name.'%')
		);

		return $get_spells->fetchAll(PDO::FETCH_ASSOC);
	}

}
