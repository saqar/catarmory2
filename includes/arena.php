<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Arenateam extends Cache {

	protected $_team;
	protected $db;

	/**
	 * @param PDO database handler
	 */
	function __construct($db) {
		$this->db = $db;
	}

	/**
	 * Get arenateam by id
	 */
        public function get_by_guid($guid) {
		// search for cached data. Set variable and stop processing when found.
		if ($this->_team = $this->get_cache(array('arenateam',$id),TEAM_EXPIRE)) {
			return;
		}


		if (intval($guid) != 0) {
			$get_team = $this->db->query('
				SELECT chat.`arenaTeamId`,chat.`name` AS arenateamName,chat.`type`,chat.`rating`,chat.`seasonGames`,chat.`seasonWins`,chat.`weekGames`,chat.`weekWins`,chat.`rank`,chat.`captainGuid`,ch.`name` AS captainName,ch.`race` AS captainRace
				FROM `'.$this->db->characterdb.'`.`arena_team` AS chat
				LEFT JOIN `'.$this->db->characterdb.'`.`characters` AS ch ON (chat.`captainGuid`=ch.`guid`)
				WHERE chat.`arenaTeamId` = ?',
				array($guid)
			);
			if ($get_team->rowCount() == 1) {
				$this->_team = $get_team->fetch(PDO::FETCH_ASSOC);
				$this->_get_team_members();
				$this->store_cache(array('arenateam',$guid),$this->_team);
			}
		}
	}

	/**
	 * Get arenateam by name
	 */
	public function get_by_name($name) {
		// search for cached data. Set variable and stop processing when found.
		if ($this->_team = $this->get_cache(array('arenateam',$name),TEAM_EXPIRE)) {
			return;
		}
		if (is_string($name)) {
			$get_team = $this->db->query('
				SELECT chat.`arenaTeamId`,chat.`name` AS arenateamName,chat.`type`,chat.`rating`,chat.`seasonGames`,chat.`seasonWins`,chat.`weekGames`,chat.`weekWins`,chat.`rank`,chat.`captainGuid`,ch.`name` AS captainName,ch.`race` AS captainRace
				FROM `'.$this->db->characterdb.'`.`arena_team` AS chat
				LEFT JOIN `'.$this->db->characterdb.'`.`characters` AS ch ON (chat.`captainGuid`=ch.`guid`)
				WHERE chat.`name` = ?',
				array($name)
			);
		}
		if ($get_team->rowCount() == 1) {
			$this->_team = $get_team->fetch(PDO::FETCH_ASSOC);
			$this->_get_team_members();
			$this->store_cache(array('arenateam',$id),$this->_team);
		}

	}

	/**
	 * Returns team informations
	 * @return array team informations
	 */
	public function get_team() {
		if (!$this->_team['arenaTeamId'])
			return;

		return $this->_team;
	}
	
	/**
	 * Returns team guid
	 * @return integer team guid
	 */
	private function _get_team_id() {
		return $this->_team['arenaTeamId'];
	}
	
	/**
	 * Get team members
	 */
	private function _get_team_members() {
		$get_members = $this->db->query('
			SELECT c.`guid`,c.`name`,c.`race`,c.`class`,atm.`weekGames`,atm.`weekWins`,atm.`seasonGames`,atm.`seasonWins`,atm.`personalRating`
			FROM `'.$this->db->characterdb.'`.`arena_team_member` AS atm
			LEFT JOIN `'.$this->db->characterdb.'`.`characters` AS c ON (atm.`guid`=c.guid)
			WHERE atm.`arenaTeamId`=?',
			array($this->_get_team_id())
		);
		$this->_team['members'] = $get_members->fetchAll(PDO::FETCH_ASSOC);
	}
}
