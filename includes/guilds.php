<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Guilds extends Cache {

	protected $db;

	/**
	 * Initialize new search
	 */
	function __construct($db) {
		$this->db = $db;
	}

	/**
	 * Search in guilds
	 * @return array guilds list
	 */
	public function search_by_name($name) {
		$get_guilds = $this->db->query('
			SELECT chg.guildid, chg.`name` AS guildName,chg.`level` AS guildLevel,chg.experience,chg.todayExperience,ch.`name` AS leaderName,ch.`race` AS leaderRace
			FROM `'.$this->db->characterdb.'`.`guild` AS chg
			LEFT JOIN `'.$this->db->characterdb.'`.`characters` AS ch ON (chg.leaderguid=ch.guid)
			WHERE chg.name COLLATE utf8_general_ci LIKE ? LIMIT '.SQL_LIMIT,		// consider adding fulltext over name filed in mysql and use AGAINST
			array('%'.$name.'%')
		);

		$guilds = $get_guilds->fetchAll(PDO::FETCH_ASSOC);

		foreach ($guilds AS &$guild) {
			$get_guild_members = $this->db->query('
				SELECT COUNT(*) AS num
				FROM `'.$this->db->characterdb.'`.`guild_member` AS chgm
				WHERE chgm.guildid=?',
				array($guild['guildid'])
			);
			$members = $get_guild_members->fetch(PDO::FETCH_ASSOC);
			$guild['members'] = $members['num'];
		}
		
		return $guilds;
	}
	
}
