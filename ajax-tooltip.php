<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


include_once "configs/config.php";
include_once "includes/database.php";
include_once "includes/shared.php";
include_once "includes/cache.php";
include_once "includes/item.php";
include_once "includes/character.php";
include_once "includes/itemset.php";
include_once "includes/glyph.php";
include_once "includes/spellitemenchantment.php";
include_once "includes/spell.php";
include_once "includes/npc.php";
include_once "includes/arena.php";
include_once "includes/guild.php";
include_once "includes/tooltip.php";


class Tooltip {

	public $error_code = 0;
	public $error_text = '';
	public $output = array();	// output before json encoding

	private $_callback = '';	// jsonp callback
	private $_cached;


	function __construct() {

	}

	public function init() {
		$this->_callback = $_GET['callback'];
		$this->db = new Database($this);

                if ($this->error_code == 0) {

			$this->data = new TooltipData($this->db);

			switch ($_GET['action']) {
				case 'get_item':
					$this->_output = $this->data->get_item((array_key_exists('guid',$_GET) ? $_GET['guid'] : NULL),(array_key_exists('entry',$_GET) ? $_GET['entry'] : NULL));
					break;
				case 'get_spell':
					$this->_output = $this->data->get_spell($_GET['entry']);
					break;
				case 'get_character':
					$this->_output = $this->data->get_character((array_key_exists('guid',$_GET) ? $_GET['guid'] : NULL),(array_key_exists('name',$_GET) ? $_GET['name'] : NULL));
					break;
				case 'get_spawn':
					$this->_output = $this->data->get_spawn($_GET['guid']);
					break;
				case 'get_guild':
					$this->_output = $this->data->get_guild((array_key_exists('guid',$_GET) ? $_GET['guid'] : NULL),(array_key_exists('name',$_GET) ? $_GET['name'] : NULL));
					break;
				case 'get_arenateam':
					$this->_output = $this->data->get_arenateam($_GET['guid']);
					break;

				default:
					$this->_throw_error(509,"Request Error","Provided request doesn't match any of internal methods");
					break;
			}
		}

		$this->output();
	}

	public function set_cached($cached) {
		$this->_cached = $cached;
	}

	public function throw_error($code,$category,$text) {
		$this->error_code = $code;
		$this->error_category = $category;
		$this->error_text = $text;
	}

	private function output() {
		if ($this->error_code != 0) {
			$error = array(
				'code' => $this->error_code,
				'category' => $this->error_category,
				'text' => $this->error_text
			);
			print $this->_callback.'('. json_encode(array('error' => $error)) .');';
		} else {
			$data = array(
				'data' => $this->_output,
				'cached' => $this->_cached,
			);

			print $this->_callback.'('. json_encode($data) .');';
		}
	}



}

error_reporting(0);
header('Content-Type: text/javascript; charset=utf8');

$armory = new Tooltip();
$armory->init();
