/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Talents class
 * @class Talents
 * @singleton
 */
 
Talents = (function(c) {
	var Talents = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Talents.prototype, {
		/**
		 * Initialize character talents
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			this.tabs = {};
			this.talents = {primary: {}, secondary: {}};
			this.glyphs = {};
			this.selectedSpec = 'primary'
		},

		/**
		 * Get talents from server and render them
		 */
		retrieveTalents: function() {
			Main.fetch(
				{
					what: 'char',
					action: 'talents',
					guid: this.character.getGuid()
				}, function(response) {
					var s = response.data;

					this.glyphs = s.glyphs;
					
					for (var i in s['tabs']) {
						this.tabs[s['tabs'][i].tree_id] = s['tabs'][i];
					
						this.talents.primary[s['tabs'][i].tree_id] = [];
						this.talents.secondary[s['tabs'][i].tree_id] = [];
					}
					
					//alert(this.talents[851])
					
					for (var i in s['talents']) {
						if (s['talents'][i].spec == 0) {
							this.talents.primary[s['talents'][i].grp].push(s['talents'][i]);
						} else {
							this.talents.secondary[s['talents'][i].grp].push(s['talents'][i]);
						}
					}
					
					this.renderTalents();
					this.renderCalculator();
					
				},this
			);
		},

		/**
		 * Get talent tree specialization depending on talents distribution
		 * @param {String} build "primary" or "secondary"
		 * @return {Object} talent tree object
		 */
		_getCharacterSpec: function(build) {	// primary or secondary
			var topCount = 0;
			var tree = {points: []};
			var counts = {};
			
			for (var i in this.talents[build]) {
				var count = 0;
				for (var j in this.talents[build][i]) {
					count += parseInt(this.talents[build][i][j].points);
				}
				counts[this.tabs[i].tree_index] = count;
				if (count > topCount) {
					topCount = count;
					tree = this.tabs[i];
					tree['points'] = [];
				}
			}
			for (var i=0;i<=2;++i) {
				tree['points'].push(counts[i]);
			}

			return tree;
		},

		/**
		 * Render talents
		 */
		renderTalents: function() {
			//var points = []
			//for (var i in this.tabs) {
			//	points.push(this.talents['primary'][i].length);
			//}
			
			var primarySpec = this._getCharacterSpec('primary');
			var secondarySpec = this._getCharacterSpec('secondary');
			if (primarySpec.tree_id) {
				$('#primary_name').html(talentTabs[primarySpec.tree_id]);
				$('#primary_icon').html('<img src="icons_32/'+(primarySpec.icon)+'.png">');
				$('#primary_points').html(primarySpec['points'].join(' / '));
			} else {
				$('#primary_name').html('No spec');
	            $('#primary_icon').html('<img src="icons_32/inv_misc_questionmark.png">');
	            $('#primary_points').html('- / - / -');

			}
			
			if (secondarySpec.tree_id) {
				var points = []
				for (var i in this.tabs) {
					points.push(this.talents['secondary'][i].length);
				}
				$('#secondary_name').html(talentTabs[secondarySpec.tree_id]);
				$('#secondary_icon').html('<img src="icons_32/'+(secondarySpec.icon)+'.png">');
				$('#secondary_points').html(points.join(' / '));
			} else {
				$('#secondary_name').html('No spec');
	            $('#secondary_icon').html('<img src="icons_32/inv_misc_questionmark.png">');
	            $('#secondary_points').html('- / - / -');

			}
		},

		_getTierColSpell: function(spec,tab,tier,col) {
			for (var i in this.talents[spec][tab]) {
				if (this.talents[spec][tab][i].tier == tier && this.talents[spec][tab][i].col == col)
					return this.talents[spec][tab][i];
			}
		},
		
		/**
		 * Calculates what arrow shape should be used between this and required talent before.  
		 * @return {Object} Arrow type and top/left position of it.
		 */
		_calculatePrerequisites: function(tabs,icon) {
			for (var i in tabs) {
 				if (tabs[i].id == icon.requires) {
					var tierDiff = icon.tier - tabs[i].tier;
					var colDiff = icon.col - tabs[i].col;
					if (tierDiff == 0 && colDiff == 1) {
						return {name: 'right', css: {left: -21, top: 10}};
					} else if (tierDiff == 0 && colDiff == -1) {
						return {name: 'left', css: {left: 32, top: 10}};
					} else if (tierDiff == 1 && colDiff == 0) {
						return {name: 'down', css: {left: 9, top: -25}};
					} else if (tierDiff == 2 && colDiff == 0) {
						return {name: '2down', css: {left: 9, top: -82}};
					} else if (tierDiff == 1 && colDiff == 1) {
						return {name: 'right_down', css: {left: -21, top: -46}};
					} else {
						return {name: 'unk', css: {left: 0, top: 0}};
					}
				}
			}
		},
		
		
		/**
		 * Render talents "calculator". It actually doesn't allow manipulating talents. 
		 */
		renderCalculator: function() {	
			var scope = this;
			Main.fetch(
				{
					what: 'talents',
					'class': this.character.getClass()
				}, function(response) {
					this.tabsData = response.data;
					this._renderSpec(this.selectedSpec);
					
					$primary = $('<div>Primary</div>');
					$primary.click(function() {
						scope._renderSpec('primary');
						this.selectedSpec = 'primary';
						$primary.addClass('active');
						$secondary.removeClass('active');
					});
					
					$secondary = $('<div>Secondary</div>');
					$secondary.click(function() {
						scope._renderSpec('secondary');
						this.selectedSpec = 'secondary';
						$secondary.addClass('active');
						$primary.removeClass('active');
						
					});
					$('#talents_spec_wrapper').html('');
					
					$('#talents_spec_wrapper').append($primary);
					$('#talents_spec_wrapper').append($secondary);

					if (this.selectedSpec == 'primary') {
						$primary.addClass('active');
					} else {
						$secondary.addClass('active');
					}
					
				},this
			);
		},
		
		_renderSpec: function(build) {


			
			
			var slots = [];
			for (var i in this.tabs) {
				slots[this.tabs[i].tree_index] = this.tabs[i];
			}
			$('#talents_tree_wrapper').html('');

			for (var i in slots) {

				var $wrapper = $('<div style="float: left; padding: 20px 20px 0 0; position: relative;"></div>');

				var slot = slots[i];
				var $name = $('<div style="color: #000">'+talentTabs[slot.tree_id]+'</div>');
				$wrapper.append($name);

				var $slot_wrapper = $('<img src="images/talents_'+this.character.klass+'_'+i+'.jpg">');
				$wrapper.append($slot_wrapper);

				var $tab_wrapper = $('<div style="position: absolute; top: 60px; left: 20px; "></div>');
				$wrapper.append($tab_wrapper);
				for (var j in this.tabsData['tab_'+slot.tree_id]) {
					var icon = this.tabsData['tab_'+slot.tree_id][j];
					var top = 57 * icon.tier;
					var left = 53 * icon.col;

					// get spell (id and points/rank) from character data
					var spell = this._getTierColSpell(build,slot.tree_id,icon.tier,icon.col);
					// if character doesnt have any points in this talent, use class default to get spell id (for tooltip - rank 1)
					if (!spell) {
						spell = {
							points: null,
							spell: icon.spell
						};
					}

					var $icon_wrapper = $('<div data-type="spell" data-entry="'+spell.spell+'"  style="position: absolute"></div>');
					$icon_wrapper.css({top: top, left: left});

					var $icon = $('<a href="spell.html#'+spell.spell+'"><img style="position: absolute; width:32px; height:32px;" src="icons_32/'+icon.icon+'.png"></a>');
					$icon_wrapper.append($icon);

					if (spell.points) {
						// with points spent in talent - make little window in right bottom corner
						var $points_wrapper = $('<div style="position: relative; top: 25px; left: 25px; background: #000; width: 10px; height: 15px; border: 1px solid #999; border-radius: 3px; line-height: 16px; text-align: center"><span style="color:#'+(spell.points < icon.max ? '1eff00' : 'e6cc80')+'">'+spell.points+'</span></div>');
						$icon_wrapper.append($points_wrapper);
					} else {
						// no points - make it greyscale
						$icon.css('-webkit-filter', 'grayscale(100%)');
					}

					if (icon.requires > 0) {
						var arrow = this._calculatePrerequisites(this.tabsData['tab_'+slot.tree_id],icon);
						var $arrow = $('<img style="position: absolute;" src="images/talent_arrow_'+arrow.name+'.png">');
						$arrow.css(arrow.css);
						$icon_wrapper.append($arrow);
					}
					
					$icon_wrapper.tooltip();
					$tab_wrapper.append($icon_wrapper);
				}
				$('#talents_tree_wrapper').append($wrapper);
		
			}
			this._renderGlyphs(build);
		},
		
		
		/**
		 * Render glyphs holder 
		 */
		_renderGlyphs: function(build) {
			$('#talents_glyphs_wrapper').html('');
			var $name = $('<div class="glyph_cat">Prime Glyphs</div>');
			$('#talents_glyphs_wrapper').append($name);
			var $ul_prime = $('<ul></ul>');
			$('#talents_glyphs_wrapper').append($ul_prime);

			var $name = $('<div class="glyph_cat">Major Glyphs</div>');
			$('#talents_glyphs_wrapper').append($name);
			var $ul_major = $('<ul></ul>');
			$('#talents_glyphs_wrapper').append($ul_major);

			var $name = $('<div class="glyph_cat">Minor Glyphs</div>');
			$('#talents_glyphs_wrapper').append($name);
			var $ul_minor = $('<ul></ul>');
			$('#talents_glyphs_wrapper').append($ul_minor);

			for (var i in this.glyphs[build]) {
				var $li = $('<li data-type="spell" data-entry="'+this.glyphs[build][i].spell_id+'">'+this.glyphs[build][i].name+'</li>');
				$li.tooltip();
				if (this.glyphs[build][i].glyph_type == 2) {
					$ul_prime.append($li);
				} else if (this.glyphs[build][i].glyph_type == 0) {
					$ul_major.append($li);
				} else if (this.glyphs[build][i].glyph_type == 1) {
					$ul_minor.append($li);
				} 
			}
		}
	});
	
	return Talents;
})();
