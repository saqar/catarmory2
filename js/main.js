/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Main class
 * @class Main
 * @singleton
 */

// Main singleton
var Main = {
	/**
	 * Initialize Application
	 * @param {string} server server name
	 * @param {string} url armory ajax PHP gateway script
	 */
	init: function(server,url) {
		this.server = server;
		this.url = url;
		
		this.auth = new Auth();
		this.initLogin();
	},

	/**
	 * Fetch data from PHP backend using JSONP request
	 * @param {Object} data Request data
	 * @param {Function} cb callback
	 * @param {Object} _scope Scope in which callback is called
	 */
	fetch: function(data,cb,_scope) {
		var scope = this;
		$.ajax({
			type: "GET",
			url: this.url,
			data: data,
			dataType: "jsonp",
			error: function() {
				alert('error while requesting ajax gateway');
			},
			success: function(json) {
				json = scope.evaluate(json);
				cb.call(_scope, json);
			}
		});
	},

	/**
	 * Returns ajax response data, when there is no error. Alerts error, when found. This is application level error - i.e. character was not found, etc.
	 * @param {Object} response Ajax response.
	 * @return {Object} Ajax response data or soft error. Or false if hard error.
	 */
	evaluate: function(response) {

		if (response.error) {
			// Error codes below 500 are application soft errors (failure on user login, etc.).
			// Error codes above 500 are hard errors (database connection/statement errors, etc.)
			if (response.error.code && parseInt(response.error.code) < 500) {
				return response;
			} else {
				alert('Armory error: '+response.error.code+' ('+response.error.category+')\nDetails: '+response.error.text);
				return false;
			}
		} else {
			// This is evaluated with every non-error ajax response from server 
			// When we have logged state, but server doesn't know us - call initLogin that tries to relog or displays the login link
			if (this.auth.isLogged() && !response.logged) {
				this.auth.setNotLogged();
				console.log('You have been logged off, trying relogin');
				this.initLogin();
			}
			
			return response;
		}
	},
	
	
	/**
	 * Show current login status and tries to relog or when not successful, displays login link 
	 */
	initLogin: function() {
		var scope = this;

		if (!this.auth.isLogged()) {

			var $notLogged = $('<div class="clickable">Not logged. Log in?</div>');
			$('#authorization').html($notLogged);

			scope.auth.check(function() {
				// function doen't return anything - returned values are parsed and stored in Auth()
				
				if (!this.auth.isLogged()) {
					// generate login form
					$notLogged.click(function() {
						var $loginForm = $('<form method="POST"><table><tr><td>Login:</td><td><input type="text" name="login"></td></tr><tr><td>Password:</td><td><input type="password" name="password"></td></tr><tr><td></td><td><input type="submit" value="Login"></td></tr></table></form>');
						var $resultState = $('<div></div>');
						
						// create dialog window filled with login form
						var window = new Window();
						window.openWindow('Login with your WoW account',[$loginForm,$resultState]);
						
						// bind submitting form to this function
						$loginForm.submit(function(event) {
							event.preventDefault();
							
							$resultState.html("Loging in... please wait");
							
							var login = $('input[name="login"]',$loginForm).val();							
							var password = $('input[name="password"]',$loginForm).val();
							$('input[name="password"]',$loginForm).val('');

							// check whether login was entered
				            if (login == '') {
				            	$resultState.html("Login can't be empty");
				            	return false;
				            }

				            // check whether password was entered
				            if (password == '') {
				            	$resultState.html("Password can't be empty");
				            	return false;
				            }

				            // get the challenge from server
				            scope.auth.getChallenge(login,function(challenge) {
			                    if (!challenge) {
			                    	$resultState.html('Login is disabled');
			                    } else {
			                    	// generate and send response to server
			                    	scope.auth.sendResponse(password,function(response) {
			                        	if (!response) {
			                        		$resultState.html('Bad login or password');
			                        	} else {
			                        		// display profile link
			                        		scope._drawLogged();
			                        		// close login dialog window
			                        		window.close();
			                        	}
			                        },this);
			                    }
				            },this);
				        });
					});

				} else {
					// server said that we are logged
					scope._drawLogged();
				}

			},scope);
		} else {
			// we are logged
			scope._drawLogged();
		}
	},
	
	/**
	 * Displays link to profile
	 */
	_drawLogged: function() {
		var scope = this;
		var $wrapper = $('<div></div>');
		var nick = this.auth.getNick();
		var $logged = $('<span style="margin-right: 10px;" class="clickable">Logged in as: '+nick.name+'</span>');
		$wrapper.append($logged);

		var $logout = $('<span class="clickable">[ Logout ]</span>');
		$wrapper.append($logout);

		$('#authorization').html($wrapper);

		// bind click event to this function
		$logged.click(function() {
			var $profileForm = $('<div>Character: </div>');
			var $form = $('<form method="POST"></form>')
			$profileForm.append($form);
			var $select = $('<select name="nick"></select>');
			$form.append($select);
			
			for (var i in scope.auth.characters) {
				var character = scope.auth.characters[i];
				var $option = $('<option value="'+character.guid+'">'+character.name+'</option>');
				if (scope.auth.getProfileValue('character_id') == character.guid) {
					$option.attr('selected', true);
				}
				$select.append($option);
			}
			
			var $submit = $('<input type="submit" value="Set">');
			$form.append($submit);
			
			var window = new Window();
			window.openWindow('Select you preferred character',[$profileForm]);
			
			$form.submit(function(event) {
				event.preventDefault();
				
				
				scope.auth.setProfile($(this),function() {
					scope.initLogin();
					window.close();
				});
			});
			
		});
		
		$logout.click(function() {
			scope.auth.logout(function() {
				scope.initLogin()
			});
		});
		
	},
	
	
	/**
	 * Get character data from server
	 * @param {string} charName character name
	 * @param {string} [activeTab] opens selected tab while character is loaded
	 */
	getChar: function(charName,activeTab) {
		var tabs = new Tabs($('#ul_menu'),false);

		for (var i in mainMenu) {
			tabs.add({
				id: mainMenu[i].name,
				name: L12N(mainMenu[i].name),
				$el: $('#'+mainMenu[i].id),
				data: function() {
					// now, when function is passed as data, it knows that $el holds pre-generated tab content
					// this needs to be revisited - probably set data to null?
				},
				onClick: function(obj) {
					Router.setParameter(1,obj.id);
				},
				_scope: this
			});
		}

		tabs.render();

		tabs.activate(activeTab ? activeTab : 'basic');

		// watch for hash changes of 1st index (activate appropriate tab)
		Router.bind(1,function(tab) {
			tabs.activate(tab);
		});

		
		this.fetch(
			{
				what: 'char',
				action: 'basic',
				name: charName
			}, function(response) {
				var c = response.data;
				// create new character object with basic data from ajax
				this.character = new Character(c);
				this.character.initBasic();
				this.character.setCached('character',response.cached);
			}, this
		);

	},

	
	/**
	 * Search for string on server. It can be character, item, guild, ...
	 * @param {string} string character name
	 * @param {string} [tab] opens selected tab when search is done.
	 */
	search: function(string,tab) {
		this.fetch(
			{
				what: 'search',
				string: string
			}, function(response) {
				var s = response.data;
				var tabs = new Tabs($('#ul_menu'),$('#search_result'));
				var firstTab = null;
				for (var i in s) {
					if (s[i].length > 0) {
						if (!firstTab) {
							firstTab = i;
						}
						var template;
						switch (i) {
							case 'characters': template = ListView.templates.characters ; break;
							case 'items': template = ListView.templates.items ; break;
							case 'guilds': template = ListView.templates.guilds ; break;
							case 'arenateams': template = ListView.templates.arenateams ; break;
							case 'npcs': template = ListView.templates.npcs ; break;
							case 'quests': template = ListView.templates.quests ; break;
							case 'gameobjects': template = ListView.templates.gameobjects ; break;
							case 'spells': template = ListView.templates.spells ; break;
							case 'achievements': template = ListView.templates.achievements ; break;
						}
						var list = new List(template,s[i]);
						tabs.add({
							id: i,
							name: L12N(i),
							data: list,
							onClick: function(obj) {
								Router.setParameter(1,obj.id);
							}
						});
					}
				};
				tabs.render();
				tabs.activate(tab ? tab : firstTab);
				
				// watch for hash changes in 2nd index - activate appropriate tab
				Router.bind(1,function(tab) {
					tabs.activate(tab);
				});
			},this
		);
	},
	
	

	/**
	 * get item data
	 * @param {Number} id Item ID
	 */
	getItem: function(id) {
		this.fetch(
			{
				what: 'item',
				id: id
			}, function(response) {
				var i = response.data;

				this.item = new Item(i);
				this.item.initBasic();
			},this
		);
	},
	

	/**
	 * Get guild from server
	 * @param {string|number} guildName guild name or guild id
	 * @param {string} [activeTab] opens selected tab while character is loaded
	 */
	getGuild: function(guildName,activeTab) {

		// fetch basic guild data
		this.fetch(
			{
				what: 'guild',
				action: 'basic',
				name: guildName
			}, function(response) {
				var c = response.data;
				// create new quild object with basic data from ajax
				this.guild = new Guild(c);
				this.guild.initBasic(activeTab);
			}, this
		);
	},

	/**
	 * Get arenateam from server
	 * @param {string|number} teamName arenateam name or arenateam id
	 * @param {string} [activeTab] opens selected tab while character is loaded
	 */
	getArenateam: function(arenateamName,activeTab) {
		var tabs = new Tabs($('#ul_menu'),false);

		for (var i in arenateamMenu) {
			tabs.add({
				id: arenateamMenu[i].name,
				name: L12N(arenateamMenu[i].name),
				$el: $('#'+arenateamMenu[i].id),
				data: function() {
					// now, when function is passed as data, it knows that $el holds pre-generated tab content
					// this needs to be revisited
				},
				onClick: function(obj) {
					Router.setParameter(1,obj.id);
				},
				_scope: this
			});
		}

		tabs.render();

		tabs.activate(activeTab ? activeTab : 'basic');

		// watch for hash changes of 1st index (activate appropriate tab)

		Router.bind(1,function(tab) {
			tabs.activate(tab);
		});
		
		// fetch basic team data
		this.fetch(
			{
				what: 'arenateam',
				action: 'basic',
				name: arenateamName
			}, function(response) {
				var c = response.data;
				// create new arenateam object with basic data from ajax
				this.arenateam = new Arenateam(c);
				this.arenateam.initBasic();
			}, this
		);
	},


	/**
	 * Get quests from server
	 * @param {number} quest id
	 */
	getQuest: function(questId) {
		
		// fetch basic quest data
		this.fetch(
			{
				what: 'quest',
				id: questId
			}, function(response) {
				var c = response.data;
				// create new quest object with basic data from ajax
				this.quest = new Quest(c);
				this.quest.initBasic();
			}, this
		);
	},

	


	/**
	 * Get npcs from server
	 * @param {number} npc entry
	 */
	getNpc: function(entry,activeTab) {

		
		// fetch basic quest data
		this.fetch(
			{
				what: 'npc',
				action: 'basic',
				id: entry
			}, function(response) {
				var c = response.data;

				var tabs = new Tabs($('#ul_menu'),$('#npc_result'));
				
				// first tab is static (for future use of mapper)
				tabs.add({
					id: 'basic',
					name: L12N('basic'),
					$el: $('#npc_basic'),
					data: function() {
						// now, when function is passed as data, it knows that $el holds pre-generated tab content
						// this needs to be revisited - probably set data to null?
					},
					onClick: function(obj) {
						Router.setParameter(1,obj.id);
					},
					_scope: this
				});
				
				// other tabs are general lists
				for (var i in npcMenu) {
					var template;
					switch (npcMenu[i].key) {
						case 'starter': template = ListView.templates.quests ; break;
						case 'ender': template = ListView.templates.quests ; break;
						case 'loot': template = ListView.templates.loot ; break;
					}
					var list = new List(template,c[npcMenu[i].key],npcMenu[i].params);
					
					
					tabs.add({
						id: npcMenu[i].name,
						name: L12N(npcMenu[i].name),
						data: list,
						onClick: function(obj) {
							Router.setParameter(1,obj.id,true);
						},
						_scope: this
					});
				}

				tabs.render();
				//if (activeTab) {
					tabs.activate(activeTab ? activeTab : 'basic');
				//}
				
				// watch for hash changes of 1st index (activate appropriate tab)
				Router.bind(1,function(tab) {
					tabs.activate(tab);
				});


				
				// create new npc object with basic data from ajax
				this.npc = new Npc(c);
				this.npc.initBasic();
			}, this
		);
	},


	/**
	 * get spell data
	 * @param {Number} id spell ID
	 */
	getSpell: function(id) {
		this.fetch(
			{
				what: 'spell',
				id: id
			}, function(response) {
				var s = response.data;
				
				this.spell = new Spell(s);
				this.spell.initBasic();
			},this
		);
	},

	/**
	 * get achievement data
	 * @param {Number} id achievement ID
	 * @param {string} [activeTab] opens selected tab while character is loaded
	 */
	getAchievement: function(id,activeTab) {

	
		
		
		
		
		
		
		this.fetch(
			{
				what: 'achievement',
				id: id
			}, function(response) {
				var a = response.data;
				
				
				
				
				var tabs = new Tabs($('#ul_menu'),$('#achievement_result'));
				
				// first tab is static (for future use of mapper)
				tabs.add({
					id: 'basic',
					name: L12N('basic'),
					$el: $('#achievement_basic'),
					data: function() {
						// now, when function is passed as data, it knows that $el holds pre-generated tab content
						// this needs to be revisited - probably set data to null?
					},
					onClick: function(obj) {
						Router.setParameter(1,obj.id);
					},
					_scope: this
				});
				
				// other tabs are general lists
				for (var i in achievementMenu) {
					
					var template;
					switch (achievementMenu[i].key) {
						case 'achievers': template = ListView.templates.achievers; break;
					}
					var list = new List(template,a[achievementMenu[i].key],achievementMenu[i].params);
					

					tabs.add({
						id: achievementMenu[i].name,
						name: L12N(achievementMenu[i].name),
						data: list,
						onClick: function(obj) {
							Router.setParameter(1,obj.id,true);
						},
						_scope: this
					});
				}

				tabs.render();

				tabs.activate(activeTab ? activeTab : 'basic');

				// watch for hash changes of 1st index (activate appropriate tab)

				Router.bind(1,function(tab) {
					tabs.activate(tab);
				});
				
				
				
				
				
				
				
				
				
				
				this.achievement = new Achievement(a);
				this.achievement.initBasic();
			},this
		);
	},
	
};







Window = (function() {
	var Window = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Window.prototype, {
		init: function() {
			this.wrapper = null;
		},

		openWindow: function(title,contents) {
			var scope = this;
			this.$wrapper = $('<div style="position: absolute; top: 0; bottom: 0; left: 0; right: 0;"></div>');
			$('body').append(this.$wrapper);
			
			var $div = $('<div style="position: relative; width: 400px; margin: 300px auto; background: black; border: 1px solid #666; box-shadow: 0 0 20px 6px #000;"></div>');

			var $title = $('<div style="background: #555; line-height: 20px; height: 21px; margin-right: 20px; padding-left: 5px; font-weight: bold">'+title+'</div>');
			$div.append($title);
			
			var $close = $('<div style="position: absolute; right: -1px; top: -1px; border: 1px solid #666; width: 20px; height: 20px; line-height: 20px; text-align: center;" class="clickable">X</div>');
			$div.append($close);
			$close.click(function() {
				scope.close();
			});
			
			this.$wrapper.append($div);
			
			var $data = $('<div style="position: relative; padding:5px"></div>');
			
			for (var i in contents) {
				$data.append(contents[i]);
			}
			
			$div.append($data);
			
		},
		
		close: function() {
			this.$wrapper.remove();
		}
		
	});

	return Window;
})();