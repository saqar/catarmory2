/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Authorization class
 * @class Auth
 * @singleton
 */


Auth = (function(a) {
	var Auth = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Auth.prototype, {
		/**
		 * Initialize Auth
		 */
		init: function() {
			this.login = null;
			this.challengeId = null;
			this.challenge = null;
			this.password = null;
			
			this.logged = false;
			this.characters = [];
			this.profile = {};
		},
		
		/**
		 * Send challenge request from server
		 */
		getChallenge: function(login,cb,_scope) {
			this.login = login.toUpperCase();

			Main.fetch(
				{
					what: 'auth',
					action: 'challenge',
					login: this.login
				}, function(response) {
					var data = response.data;
					if (data) {
						this.challengeId = data.id;
						this.challenge = data.challenge.toUpperCase();
						cb.call(_scope, true);
					} else {
						cb.call(_scope, false);
					}
				}, this
			);
		},
		
		/**
		 * Calculate and send challenge response proof to server
		 */
		sendResponse: function(password,cb,_scope) {
			// Generate same sha_pass_hash value as it is on server
			var shaPassHash = CryptoJS.SHA1(this.login+':'+
					password.toUpperCase()
					).toString(CryptoJS.enc.Hex).toUpperCase();

			// discard password variable (security)
			password = null;

			// Generate response by hashing shaPassHash with challenge string.
			// Server can reproduce this by finding user by username and SHA1(CONCAT(UPPER(`trinity_auth`.`account`.`sha_pass_hash`),':',UPPER(`catarmory`.`challenges`.`challenge`)))
			// When these two matches, login is considered as successful.
			// If someone gets this response, he can only login to catarmory as this user. He can get the neither to the sha_pass_hash, nor account password. Even with knowing challenge and username variables.
			var response = CryptoJS.SHA1(shaPassHash+':'+this.challenge);
			var responseString = response.toString(CryptoJS.enc.Hex).toUpperCase()
			
			Main.fetch(
				{
					what: 'auth',
					action: 'response',
					challenge_id: this.challengeId,
					response: responseString
				}, function(response) {
					if (response.error) {
						this.error = response.error;
						scope.logged = false;
						cb.call(_scope, false);
					} else {
						var data = response.data;
						// array of characters {name,race,class,level}
						setCookie('challenge',this.challengeId,7);
						setCookie('response',responseString,7);
						this.characters = data.characters;
						this.profile = data.profile;
						this.logged = true;
						cb.call(_scope, true);
					}
				}, this
			);
		},
		
		/**
		 * Send session check request to server. Server responds with characters list and profile settings if session is valid
		 */
		check: function(cb,_scope) {
			var scope = this;
			Main.fetch(
				{
					what: 'auth',
					action: 'check',
				}, function(response) {
					var l = response.data;
					if (response.logged) {
						scope.logged = true;
						scope.characters = l.characters;
						scope.profile = l.profile;
					} 
					cb.call(_scope);
				}, this
			);				
						
		},
		
		/**
		 * Return actual login state
		 * @return {boolean} True if user is logged - falseo therwise
		 */
		isLogged: function() {
			return this.logged;
		},
		
		/**
		 * Set user as not logged in
		 */
		setNotLogged: function() {
			this.logged = false;
			this.characters = [];
		},

		/**
		 * Send logout request to server and delete cookies 
		 */
		logout: function(cb,_scope) {
			setCookie('challenge','',-1);
			setCookie('response','',-1);
			this.logged = false;
			this.characters = [];
			
			Main.fetch(
				{
					what: 'auth',
					action: 'logout',
				}, function() {
					cb.call(_scope);
				}, this
			);
		},
		
		/**
		 * Get currect logged character from profile
		 * @return {Object} Character object or unknown character object if no character is set
		 */
		getNick: function() {
			if (this.getProfileValue('character_id') > 0) {
				for (var i in this.characters) {
					if (this.characters[i].guid == this.getProfileValue('character_id'))
						return this.characters[i];
				}
			} else {
				return { guid: 0, name: 'Unknown character' };
			}
		},
		
		/**
		 * Get profile value by key
		 * @param {String} key Profile object key
		 * @return {String} Profile value
		 */
		getProfileValue: function(key) {
			return this.profile[key];
		},
		
		/**
		 * Send current profile settings to server
		 * @param {Object} $form jQuery form element with settings
		 * @param {Object} cb Callback function
		 * @param {Object} _scope Fucntion scope 
		 */
		setProfile: function($form,cb,_scope) {
			var scope = this;
			var postObj = {
				what: 'auth',
				action: 'set_profile',
			};
			
			// append form data to postObj object
			data = $form.serializeArray();
			for (var i in data) {
				postObj[data[i]['name']] = data[i]['value'];
			}

			Main.fetch(postObj, function(response) {
					scope.profile = response.data.profile;
					cb.call(_scope);
				}, this
			);				
						
		},
		
	});
	return Auth;
})();

