/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
/**
 * Character class
 * @class Character
 * @singleton
 */
Character = (function(c) {
	var Character = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Character.prototype, {
		/**
		 * Initialize character
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.guid = c.guid;
			this.name = c.name;
			this.race = c.race;
			this.klass = c['class'];
			this.level = c.level;
			this.money = c.money;
			this.p_faction = (1<<(c.race-1) & racemaskAlliance ? 1 : 0);
			this.guildId = c.guildid;
			this.guildName = c.guildName;
			this.guildRank = c.guildRank;
			this.cached = {};
		},

		/**
		 * Init basic stuff of a character
		 */
		initBasic: function() {
			
			var inventory = new Inventory(this);
			inventory.retrieveInventory();
			var skills = new Skills(this);
			skills.retrieveSkills();
			var talents = new Talents(this);
			talents.retrieveTalents();
			this.initQuests();
			this.initAchievements();
			this.initAuctions();
			this.initMailbox();

			
		// header (name, banner color, guild, ...)
			$('#flag').css('background','url(\'images/b_'+this.p_faction+'.jpg\')');
			$('.char_inventory').css("background", 'url(\'images/inventory_'+this.p_faction+'.jpg\')');
			$('#char_name2').html(this.name);
			$('#char_race').html('<img src="images/race_'+this.race+'.png">');
			$('#char_class').html('<img src="images/class_'+this.klass+'.png">');
			$('#char_level').html(this.level);
			if (this.guildId) {
				$('#char_guild_name').html('<a href="guild.html#'+this.guildName+'/basic">'+this.guildName+'</a>');
				$('#char_guild_rank').html('&lt;'+this.guildRank+'&gt;');
			} else {
				$('#char_guild_name').html(L12N('not_in_guild'));
			}
			
			if (typeof(this.money) !== "undefined" && this.money) {
				var money = new Money(this.money);
				$('#char_money').html(money.getHTMLMoney());
			} else {
				$('#char_money').html('Money is hidden');
			}
			
			
			// power bar
			$('#power_bar').removeClass();
			if (this.klass == 1) { //rage
				$('#power_name').html('Rage');
				$('#power_bar').addClass('rage_bar');
			} else if (this.klass == 2 || this.klass == 3 || this.klass == 5 || this.klass == 7 || this.klass == 8 || this.klass == 9 || this.klass == 11) { //mana
				$('#power_name').html('Mana');
				$('#power_bar').addClass('mana_bar');
			} else if (this.klass == 4) { //energy
				$('#power_name').html('Energy');
				$('#power_bar').addClass('energy_bar');
			} else if (this.klass == 6) { // runic
				$('#power_name').html('Runic');
				$('#power_bar').addClass('runic_bar');
			}
		},
		
		/**
		 * Initialize quests categories in character quests tab
		 */
		initQuests: function() {
			var quests = new Quests(this);
			
			var scope = this; 
			var $ul = $('<ul></ul>');
			for (var i=0; i<=4; ++i) {
				$main_zone = $('<li><div style="font-weight: bold">'+L12N('main_zone_'+i)+'</div></li>');
				$ul.append($main_zone);
				
				var $sub_zone = $('<ul></ul>'); 
				$main_zone.append($sub_zone);
				
				for (var j in zones[i]) {
					$zone = $('<li data-value="'+zones[i][j]+'">'+L12N('zone_'+zones[i][j])+'</li>');
					$sub_zone.append($zone);
					$zone.click(function() {
						var zone = $(this).attr('data-value');
						quests.retrieveZone(zone);
					})
				}
				
				
			}
			$ul.treeview({
				collapsed: true,
			});		// 3rd party treeview
			
			$('#quests_categories').prepend($ul);
		},
		
		/**
		 * Initialize achievements
		 */
		initAchievements: function() {
			var achievements = new Achievements(this);
			achievements.renderCategories($('#achievements_categories'));
		},
		
		/**
		 * Initialize auctions
		 */
		initAuctions: function() {
			var auctions = new Auctions(this);
			auctions.retrieveAuctions();
		},
		
		/**
		 * Initialize mailbox
		 */
		initMailbox: function() {
			var mailbox = new Mailbox(this);
			mailbox.retrieveMailbox();
		},
		

		/**
		 * Return character guid
		 * @return Number guid
		 */
		getGuid: function() {
			return this.guid;
		},
		
		/**
		 * Return character class
		 * @return Number class
		 */
		getClass: function() {
			return this.klass;
		},
		
		setCached: function(what,cacheInfo) {
			this.cached[what] = cacheInfo;
			html = 'Cache expiration: ';
			for (var i in this.cached) {
				if (this.cached[i])
					html += i+' at '+human_date_time(this.cached[i][0]+this.cached[i][1])+', ';
				else
					html += i+' fresh data, ';
			}
			$('#status').html(html);
		}
		
	});

	return Character;
})();

/**
 * Skills class
 * @class Skills
 * @singleton
 */
 
Skills = (function(c) {
	var Skills = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Skills.prototype, {
		/**
		 * Initialize character skills
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			this.skills = [];
		},

		/**
		 * Get skills from server and call renderSkills()
		 */
		retrieveSkills: function() {
			Main.fetch(
				{
					what: 'char',
					action: 'skills',
					guid: this.character.getGuid()
				}, function(response) {
					var s = response.data;
					for (var i in s) {
						if (typeof(this.skills[s[i]['grp']]) === "undefined") {
							this.skills[s[i]['grp']] = [];
						}
						this.skills[s[i]['grp']].push(s[i]);
					}
					this.renderSkills();
				},this
			);
		},
		
		/**
		 * Render skills on character basic tab
		 */
		renderSkills: function() {
			var out ='';
			if (this.skills[11]) {
				for (var i=0;i<this.skills[11].length;++i) {
					out += '<div class="tradeskill_wrapper"><div class="tradeskill"><img style="float:left; padding-right: 5px;" src="icons_14/'+this.skills[11][i].icon+'.png"><div style="color: black; float: left;">'+this.skills[11][i].name+'</div><div style="color: #666; float: right;">';
					out += this.skills[11][i].value+' '+' / '+this.skills[11][i].max;
					out += '</div><div class="clear"></div></div></div>';
				}
			}
			$('#tradeskills_recap').html(out);
		}
	});
	
	return Skills;
})();





/**
 * Achievements class
 * @class Achievements
 * @singleton
 */
 
Achievements = (function(c) {
	var Achievements = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Achievements.prototype, {
		/**
		 * Initialize character Achievements
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			this.tree = {};
		},
		
		/**
		 * Get character achievements categories from server and render them in character achievements tab (left pane)
		 */
		renderCategories: function($el) {	
			var scope = this;
			
			Main.fetch(
				{
					what: 'achievements',
					action: 'char'
				}, function(response) {
					var c = response.data;
					var $ul = $('<ul></ul>');
					for (var i in c) {
						var $li = $('<li></li>');
						var $div = $('<div data-value="'+c[i].id+'">'+c[i].name+'</div>');
						$div.click(function() {
							var category = $(this).attr('data-value');
							scope.retrieveCategory(category);
						})
						$li.append($div);
						$ul.append($li);

						if (c[i].childs.length > 0) {
							var $ul2 = $('<ul></ul>');
							for (var j in c[i].childs) {
								var $li2 = $('<li data-value="'+c[i].childs[j].id+'">'+c[i].childs[j].name+'</li>');
								$li2.click(function() {
									var category = $(this).attr('data-value');
									scope.retrieveCategory(category);
								})
								$ul2.append($li2);
							}
							$li.append($ul2);
						}
						
					}
					$el.prepend($ul);
					$el.treeview({
						collapsed: true,
					});		// 3rd party treeview
				},this
			);
		},

		/**
		 * Get achievements from server and render them
		 */
		retrieveCategory: function(category) {
			// fetch completed achievements by this character in this category.
			Main.fetch(
				{
					what: 'char',
					action: 'achievements',
					guid: this.character.getGuid(),
					category: category
				}, function(response) {
					var completedAchievements = response.data;

					// Fetch all quests in the zone with their tieles, descriptions, rewards, etc.
					Main.fetch(
						{
							what: 'achievements',
							action: 'by_category',
							category: category
						}, function(response) {
							var achievements = response.data;

							$('#achievements_pane').html('');
							
							// use List to render grid of achievements, but hide column `Category` (which is not necessary - whole list is in same category).
							// And add column `Completed` showing yes if achievement id presents on completed list
							var list = new List(ListView.templates.achievements,achievements,{
								hide: ['category'],
								add: [
								    {
								    	id: 'Completed',
								    	name: 'Completed',
										align: 'center',
										text: function(t,d,s) {
											if (a = search_in_array_key(d.id,s.additionalData.achievements,'achievement')) {
												return '<div style="color: #1eff00">Yes</div><div style="font-size: 12px">'+human_date(a.date)+'</div>';
											}
										}
								   }
								],
								additionalData: {
									achievements: completedAchievements
								}
								
							});
							list.render($('#achievements_pane'));
						},this
					);
				
				},this
			);
		},
	});
	return Achievements;
})();




/**
 * Auctions class
 * @class Auctions
 * @singleton
 */
 
Auctions = (function(c) {
	var Auctions = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Auctions.prototype, {
		/**
		 * Initialize character Auctions
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			
		},

		/**
		 * Get achievements from server and render them
		 */
		retrieveAuctions: function() {
			Main.fetch(
				{
					what: 'char',
					action: 'auctions',
					guid: this.character.getGuid()
				}, function(response) {
					var auctions = response.data;
					$('#auctions_pane').html('');
					var list = new List(ListView.templates.auctions,auctions,{});
					list.render($('#auctions_pane'));
				},this
			);
		},
	});
	return Auctions;
})();




/**
 * Mailbox class
 * @class Mailbox
 * @singleton
 */
 
Mailbox = (function(c) {
	var Mailbox = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Mailbox.prototype, {
		/**
		 * Initialize character Auctions
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			
		},

		/**
		 * Get achievements from server and render them
		 */
		retrieveMailbox: function() {
			Main.fetch(
				{
					what: 'char',
					action: 'mailbox',
					guid: this.character.getGuid()
				}, function(response) {
					var mailbox = response.data;
					$('#mailbox_pane').html('');
					var list = new List(ListView.templates.mailbox,mailbox,{});
					list.render($('#mailbox_pane'));
				},this
			);
		},
	});
	return Mailbox;
})();



